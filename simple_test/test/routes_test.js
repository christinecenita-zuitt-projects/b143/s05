const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	const domain = 'http://localhost:5001';
	it('GET / endpoint', (done)=>{
		chai.request(domain) 
		.get('/')
		.end((error, res) => { 
			assert.equal(res.status, 200);
			done(); 
		}) 
	})

	it('GET /rates endpoint', (done)=>{
		chai.request(domain)
		.get('/currency')
		.end((err, res) =>{
			expect(res).to.not.equal(undefined);
			done()
		})
	})

	it('POST /currency endpoint', (done)=>{
		chai.request(domain)
		.post('/currency')
		.type('json') 
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh'
		})
		.end((err, res) =>{
			expect(res.status).to.equal(200);
			done();
		})
	})

	it('POST /currency endpoint name is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			'alias': "riyadh"
			
		})
		.end((err, res) =>{
			expect(res.status).to.equal(400); 
			done()
		})
	})


	it('POST /currency endpoint name is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh'
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint name is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint ex is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint ex is not an object', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint ex is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint alias is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			name: 'Saudi Arabian Riyadh'
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint alias not an string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			name: 'Saudi Arabian Riyadh'
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})

	it('POST /currency endpoint alias is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			name: 'Saudi Arabian Riyadh'
		})
		.end((err, res)=> {
			assert.equal(res.status, 400);
			done();
		})
	})
})      
       


