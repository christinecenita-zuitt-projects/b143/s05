const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency
		
		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
